# multi-conteneurs

Clonez le projet avec la commande `git clone` :

```sh
git clone --recurse-submodules https://gitlab.com/cours-docker1/node-docker.git

sh build.sh
```

Nous avons donc dans ce repo git et un lien vers deux autres repo :

- Le front (avec lequel vous avez intéragi avec votre Dockerfile)
- Un back (Le dockerfile est déjà fait par mes soins)

À la racine de notre projet `node-docker`, créez un fichier de déclaration appelé `docker-compose.yml`. En voici un exemple :

```yaml
version: "3"
services:
    front:
        build: ./path/to/context
        ports: 
            - '<port_host>:<port_container>'
        volumes: 
            - '<folder_host>:<folder_container>'
        depends_on:
            - <service_name>
        environment:
            - EXAMPLE=changeme

```

Plusieurs remarques : 

- La première ligne après `services` déclare le conteneur de notre application.

- les lignes suivantes permettent de décrire comment lancer notre conteneur.

- `build` spécifie comment Docker doit créer une image pour un service en utilisant un Dockerfile ou un contexte de construction spécifique.

- `ports` permet de binder l'accès hôte -> container afin d'accéder au container.

- `volumes` est un attribut qui permet d'utiliser du stockage persistant au dela de la vie du container.

- `depends_on` attend que les services listés soient démarrés avant de se lancer.

- `environments` permet de set des variables d'environnement dans le container du service.

Il existe beaucoup d'autres attributs différents pour personnalisé le conteneur.

Ce que je vous demande aujourd'hui, c'est de me faire un docker-compose fonctionnel avec lequel le front et le back vont pouvoir communiquer de pair.

Dans mon docker-compose je souhaite retrouver : 

- deux `services` dont le nom sont `front` et `back`

Pour le front : 

- nom du conteneur: node-front-starter
- build: path-to-the-dockerfile-front
- Port_host: 3000
- Port_conteneur: 3000
- un volume dont le folder source est la racine du projet front, et le target le folder /usr/src/app du conteneur.
- un répertoire de travail ciblé sur /usr/src/app
- une dépendance au service back
- des variables d'environnement :
    - `REACT_APP_VAR_ENV` = <string_de_votre_choix>
    - `REACT_APP_BACK_URL`= http://<service_back_name>:<port>

`
Pour le back :

- nom du conteneur: node-back-starter
- build: path-to-the-dockerfile-back
- Port_host: 3001
- Port_conteneur: 3001
- un volume dont le folder source est la racine du projet back, et le target le folder /usr/src/app du conteneur.
- un répertoire de travail ciblé sur /usr/src/app